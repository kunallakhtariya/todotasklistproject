package com.example.webwerks.todolisttaskdemo.listener;

/**
 * Created by webwerks on 12/1/16.
 */
public interface DateTimeListener  {

    public void onDateSet(String date);
    public void onTimeSet(int hour,int minute);
}
