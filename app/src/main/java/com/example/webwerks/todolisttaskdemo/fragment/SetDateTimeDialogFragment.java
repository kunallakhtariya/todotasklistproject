package com.example.webwerks.todolisttaskdemo.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import com.example.webwerks.todolisttaskdemo.R;
import com.example.webwerks.todolisttaskdemo.TaskListApplication;
import com.example.webwerks.todolisttaskdemo.listener.DateTimeListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by webwerks on 5/1/16.
 */
public class SetDateTimeDialogFragment extends DialogFragment implements DateTimeListener {
    private ViewPager viewPager;
    private SimpleDateFormat dateFormatter;

    private static DateTimeListener listener;

    public static SetDateTimeDialogFragment newInstance(DateTimeListener l){
      //  listener=l;
        return new SetDateTimeDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        listener=(DateTimeListener)getActivity();
        View rootView = inflater.inflate(R.layout.set_date_time_fragment_layout, container, false);
        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Date"));
//        tabLayout.addTab(tabLayout.newTab().setText("Time"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        viewPager = (ViewPager) rootView.findViewById(R.id.pager);
        MyAdapter adapter = new MyAdapter(getChildFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {


            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return rootView;
    }

    @Override
    public void onDateSet(String date) {

        listener.onDateSet(date);
    }

    @Override
    public void onTimeSet(int hour, int minute) {
        listener.onTimeSet(hour,minute);
    }

//    @Override
//    public void onTimeSet(String time) {
//
//        listener.onTimeSet(time);
//
//    }


    class MyAdapter extends FragmentStatePagerAdapter {

        public MyAdapter(FragmentManager fm, int tabCount) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            ;
            Fragment fragment = null;
            if (position == 0) {
                fragment = DateFragment.newInstance(SetDateTimeDialogFragment.this
                );

            }
//            if (position == 1) {
//                fragment =  TimeFragment.newInstance(SetDateTimeDialogFragment.this);
//            }

            return fragment;
        }

        @Override
        public int getCount() {

            return 1;
        }

    }
}