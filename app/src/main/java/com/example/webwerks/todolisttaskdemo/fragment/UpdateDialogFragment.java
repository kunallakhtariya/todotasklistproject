package com.example.webwerks.todolisttaskdemo.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.webwerks.todolisttaskdemo.R;
import com.example.webwerks.todolisttaskdemo.TaskListApplication;
import com.example.webwerks.todolisttaskdemo.adapter.AlarmPreferenceListAdapter;
import com.example.webwerks.todolisttaskdemo.database.DataBase;
import com.example.webwerks.todolisttaskdemo.interfaces.ActionReceiver;
import com.example.webwerks.todolisttaskdemo.model.Alarm;
import com.example.webwerks.todolisttaskdemo.model.AlarmPreference;
import com.example.webwerks.todolisttaskdemo.model.Task;
import com.example.webwerks.todolisttaskdemo.ui.AlarmPreferencesActivity;
import com.example.webwerks.todolisttaskdemo.ui.MainActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by webwerks on 4/1/16.
 */
public class UpdateDialogFragment extends DialogFragment implements View.OnClickListener{

    Button button;
    EditText editText;
    Task task;
    ImageView imgbtn,imgTime;
    TextView updateDate;
    TextView updateTime;
    static ActionReceiver mReceiver;
    private SimpleDateFormat dateFormatter;
    private FragmentManager fragmentManager;
    private Alarm alarm;


    public static UpdateDialogFragment newInstance(ActionReceiver receiver,Task task){

        UpdateDialogFragment frag=new UpdateDialogFragment();
        mReceiver=receiver;
        Bundle data=new Bundle();
        data.putSerializable("object", task);
        frag.setArguments(data);
        return frag;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView=inflater.inflate(R.layout.update_task_fragment, container, false);
        editText=(EditText)rootView.findViewById(R.id.update_task);
        updateDate=(TextView)rootView.findViewById(R.id.update_task_date);
        updateTime=(TextView)rootView.findViewById(R.id.update_task_time);
        button=(Button)rootView.findViewById(R.id.update);
        imgbtn=(ImageView)rootView.findViewById(R.id.img_date);
        imgTime=(ImageView)rootView.findViewById(R.id.img_time);
        task= (Task) getArguments().getSerializable("object");
        editText.setText(task.getTaskName());
        updateDate.setText(task.getTaskDate());
        updateTime.setText(task.getTaskTime());
        button.setOnClickListener(this);
        imgbtn.setOnClickListener(this);
        imgTime.setOnClickListener(this);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        getDialog().setTitle("Update Task");
        getDialog().show();
        return rootView;
    }

    @Override
    public void onClick(View v) {
        Alarm alarm=new Alarm();
        switch (v.getId()) {
            case R.id.update:
            task.setTaskName(editText.getText().toString());
                task.setTaskDate(updateDate.getText().toString());
                task.setTaskTime(updateTime.getText().toString());
                alarm=DataBase.getAlarm(task.getId());
                alarm.setAlarmName(editText.getText().toString());
                alarm.setAlarmTime(updateTime.getText().toString());
                alarm.setAlarmTaskId(task.getId());

                Log.d("getAlarm", alarm.getAlarmTaskId() + " " + alarm.getAlarmName() + alarm.getAlarmTime());
            //  task.setId(editText.getText().toString());
            Log.d("taskname", task.getTaskName());
            Log.d("InDialog", "dialog");
                DataBase.update(alarm);
                MainActivity.instance().callMathAlarmScheduleService();
                Toast.makeText(getContext(), alarm.getTimeUntilNextAlarmMessage(), Toast.LENGTH_LONG).show();

                TaskListApplication.newInstance().updateTask(task);
            mReceiver.onActionComplete(1);
            getDialog().dismiss();
        break;

            case R.id.img_date:
                Calendar newCalendar = Calendar.getInstance();
              DatePickerDialog datePickerDialogatePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        updateDate.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialogatePickerDialog.show();
                break;
            case R.id.img_time:


                final Calendar newTime = Calendar.getInstance();
                final Alarm finalAlarm = alarm;
                TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker timePicker, int hours, int minutes) {
                        newTime.set(Calendar.HOUR_OF_DAY, hours);
                        newTime.set(Calendar.MINUTE, minutes);
                        newTime.set(Calendar.SECOND, 0);
                        finalAlarm.setAlarmTime(newTime);
                        String time=hours+":"+minutes;
                        updateTime.setText(time);

                    }
                }, newTime.get(Calendar.HOUR_OF_DAY), newTime.get(Calendar.MINUTE), true);

                timePickerDialog.show();
        }
    }






}


