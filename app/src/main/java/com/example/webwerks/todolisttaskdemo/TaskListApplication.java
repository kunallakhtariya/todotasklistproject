package com.example.webwerks.todolisttaskdemo;

/**
 * Created by webwerks on 31/12/15.
 */

import android.app.Application;
import android.util.Log;

import com.example.webwerks.todolisttaskdemo.database.DataBase;
import com.example.webwerks.todolisttaskdemo.model.Task;

import java.util.ArrayList;

public class TaskListApplication extends Application {

    ArrayList<Task> currentTaskList;
    private DataBase dataBase;
    private static TaskListApplication taskListApplication;


    @Override
    public void onCreate() {
        super.onCreate();
        taskListApplication = this;
        dataBase = new DataBase(this);
        if (null == currentTaskList) {

            currentTaskList = new ArrayList<>();
        }
        initTaskList();
    }

    public static TaskListApplication newInstance() {
        return taskListApplication;
    }

    private void initTaskList() {
        currentTaskList = dataBase.getAllData();

    }
    public void changeTaskList(){
        currentTaskList = dataBase.getAllData();
    }

    public ArrayList<Task> getCurrentTaskList() {
        return currentTaskList;
    }



    public void updateTask(Task task) {
        Log.d("FirstTime","In update Task");
        dataBase.updateTask(task);

    }

    public void insertFlag(Task task){
        dataBase.insertFlag(task);
        currentTaskList = dataBase.getAllData();
    }

    public long insertTask(Task task) {
        long id = dataBase.insertData(task);
        currentTaskList.add(task);
        return id;

    }

    public void deleteTask(int position) {
        Log.d("TaskLIstApplication","TaskLIstApplication");
        dataBase.deleteTask(currentTaskList.get(position));
        currentTaskList.remove(position);
    }

    public void completeTask(Task task){
        dataBase.deleteTask(task);
        currentTaskList.remove(task);
    }


}
