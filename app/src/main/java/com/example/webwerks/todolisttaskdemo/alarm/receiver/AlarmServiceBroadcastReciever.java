package com.example.webwerks.todolisttaskdemo.alarm.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.webwerks.todolisttaskdemo.alarm.service.AlarmService;
import com.example.webwerks.todolisttaskdemo.model.Task;

/**
 * Created by webwerks on 25/1/16.
 */
public class AlarmServiceBroadcastReciever  extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("AlarmServiceBroadcastReciever", "onReceive()");

        Intent serviceIntent = new Intent(context, AlarmService.class);

        context.startService(serviceIntent);
    }
}
