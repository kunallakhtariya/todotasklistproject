package com.example.webwerks.todolisttaskdemo.fragment;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.webwerks.todolisttaskdemo.R;
import com.example.webwerks.todolisttaskdemo.TaskListApplication;
import com.example.webwerks.todolisttaskdemo.listener.DateTimeListener;

import java.util.Calendar;

/**
 * Created by webwerks on 5/1/16.
 */
public class TimeFragment extends Fragment {

    TimePicker timePicker;
    int hour;
    int minute;
    String time;
    private static DateTimeListener listener;

    public static TimeFragment newInstance(DateTimeListener l){
        listener=l;
        return new TimeFragment();
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView= (ViewGroup) inflater.inflate(R.layout.time_fragment_layout, container, false);

        timePicker=(TimePicker)rootView.findViewById(R.id.timePicker);
        timePicker.setIs24HourView(true);
        hour=timePicker.getCurrentHour();
        minute=timePicker.getCurrentMinute();
        timePicker.setOnTimeChangedListener(timeChangedListener);
        return  rootView;
    }

   private TimePicker.OnTimeChangedListener timeChangedListener=new TimePicker.OnTimeChangedListener(){

        @Override
        public void onTimeChanged(TimePicker timePicker, int i, int i1) {

            hour=i;
            minute=i1;
//            time=hour+" : "+minute;
            listener.onTimeSet(hour,minute);

//            Toast.makeText(getContext(),time,Toast.LENGTH_SHORT).show();

        }
    };
}
