package com.example.webwerks.todolisttaskdemo.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.Toast;

import com.example.webwerks.todolisttaskdemo.R;
import com.example.webwerks.todolisttaskdemo.TaskListApplication;
import com.example.webwerks.todolisttaskdemo.listener.DateTimeListener;
import com.example.webwerks.todolisttaskdemo.model.Task;

import java.util.Calendar;

/**
 * Created by webwerks on 5/1/16.
 */
public class DateFragment extends Fragment {

    DatePicker datePicker;
    int year;
    int month;
    int day;
    String date;
    private static DateTimeListener listener;

    public static DateFragment newInstance(DateTimeListener l){
        listener=l;
        return new DateFragment();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.date_fragment_layout, container, false);

        datePicker = (DatePicker) rootView.findViewById(R.id.datePicker);
        Calendar c = Calendar.getInstance();

//        year = c.get(Calendar.YEAR);
//        month = c.get(Calendar.MONTH);
//        day = c.get(Calendar.DAY_OF_MONTH);
        year=datePicker.getYear();
        month=datePicker.getMonth();
        day=datePicker.getDayOfMonth();

        datePicker.init(year, month, day, dateSetListener);
        return rootView;
    }
    private DatePicker.OnDateChangedListener dateSetListener = new DatePicker.OnDateChangedListener() {

        public void onDateChanged(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
            monthOfYear=monthOfYear+1;
            Calendar c = Calendar.getInstance();
            c.set(year, monthOfYear, dayOfMonth);
//            int id=view.getId();
            date=dayOfMonth+"/"+monthOfYear+"/"+year;


//            String getdate=task.getTaskDate();
//            Toast.makeText(getContext(),id+"",Toast.LENGTH_SHORT).show();
//            Toast.makeText(getContext(), getdate, Toast.LENGTH_SHORT).show();
//            TaskListApplication.newInstance().updateTask(task);
//            Toast.makeText(getContext(),date,Toast.LENGTH_SHORT).show();
            listener.onDateSet(date);


        }
    };
}
