package com.example.webwerks.todolisttaskdemo.database;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.example.webwerks.todolisttaskdemo.adapter.TaskListAdapter;
import com.example.webwerks.todolisttaskdemo.model.Alarm;
import com.example.webwerks.todolisttaskdemo.model.Task;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.nio.channels.DatagramChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by webwerks on 15/12/15.
 */
public class DataBase  {

    static DataBase instance = null;
    static SQLiteDatabase database = null;
    DataBaseHelper helper;
    private ListView listView;
    private ArrayList<Task> arrayList;
    private TaskListAdapter taskListAdapter;

    public DataBase(Context context){
        helper=new DataBaseHelper(context);
    }

    public  long insertData(Task object){

        SQLiteDatabase db=helper.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(DataBaseHelper.TASK_ID,object.getId());
        contentValues.put(DataBaseHelper.TASK_NAME,object.getTaskName());
        contentValues.put(DataBaseHelper.TASK_TIME,object.getTaskTime());
        contentValues.put(DataBaseHelper.TASK_DATE, object.getTaskDate());
        contentValues.put(DataBaseHelper.TAST_FLAG, object.getCheckedFlag());
        long id=db.insert(DataBaseHelper.TABLE_NAME,null,contentValues);
        db.close();
        return id;
    }
    public static void init(Context context) {
        if (null == instance) {
            instance = new DataBase(context);
        }
    }

    public static SQLiteDatabase getDatabase() {
        if (null == database) {
            database = instance.helper.getWritableDatabase();
        }
        return database;
    }

    public static void deactivate() {
        if (null != database && database.isOpen()) {
            database.close();
        }
        database = null;
        instance = null;
    }

    public void deleteTask(Task task){
        Log.d("DeleteTask", "DeleteTask");
        SQLiteDatabase sqLiteDatabase=helper.getWritableDatabase();
        sqLiteDatabase.delete(DataBaseHelper.TABLE_NAME, DataBaseHelper.TASK_ID + " = ?", new String[]{String.valueOf(task.getId())});
       sqLiteDatabase.close();
    }

    public void insertFlag(Task object){

        SQLiteDatabase sqLiteDatabase=helper.getWritableDatabase();
        ContentValues contentValues=new ContentValues();

//        contentValues.put(DataBaseHelper.TASK_NAME,object.getTaskName());
//        contentValues.put(DataBaseHelper.TASK_TIME, object.getTaskTime());
//        contentValues.put(DataBaseHelper.TASK_DATE, object.getTaskDate());
        contentValues.put(DataBaseHelper.TAST_FLAG, object.getCheckedFlag());
        sqLiteDatabase.update(DataBaseHelper.TABLE_NAME, contentValues, DataBaseHelper.TASK_ID + " = ?", new String[]{object.getId()});
        sqLiteDatabase.close();

    }



    public void updateTask(Task object){
        Log.d("IN database","updatedatabse");

        SQLiteDatabase sqLiteDatabase=helper.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put(DataBaseHelper.TASK_NAME,object.getTaskName());
        contentValues.put(DataBaseHelper.TASK_TIME, object.getTaskTime());
        contentValues.put(DataBaseHelper.TASK_DATE, object.getTaskDate());
        contentValues.put(DataBaseHelper.TAST_FLAG, object.getCheckedFlag());
       int count= sqLiteDatabase.update(DataBaseHelper.TABLE_NAME, contentValues, DataBaseHelper.TASK_ID + " = ?", new String[]{object.getId()});
        Log.d("Task count", "" + count);
//        Log.d("Task ID", object.getId());
        sqLiteDatabase.close();
    }

    public static long create(Alarm alarm) {
        ContentValues cv = new ContentValues();
        cv.put(DataBaseHelper.COLUMN_ALARM_ACTIVE, alarm.getAlarmActive());
        cv.put(DataBaseHelper.COLUMN_ALARM_TIME, alarm.getAlarmTimeString());
        cv.put(DataBaseHelper.COLUMN_ALARM_TASK_ID,alarm.getAlarmTaskId());

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = null;
            oos = new ObjectOutputStream(bos);
            oos.writeObject(alarm.getDays());
            byte[] buff = bos.toByteArray();

            cv.put(DataBaseHelper.COLUMN_ALARM_DAYS, buff);

        } catch (Exception e){
        }

        cv.put(DataBaseHelper.COLUMN_ALARM_DIFFICULTY, alarm.getDifficulty().ordinal());
        cv.put(DataBaseHelper.COLUMN_ALARM_TONE, alarm.getAlarmTonePath());
        cv.put(DataBaseHelper.COLUMN_ALARM_VIBRATE, alarm.getVibrate());
        cv.put(DataBaseHelper.COLUMN_ALARM_NAME, Task.getInstance().getTaskName());

        return getDatabase().insert(DataBaseHelper.ALARM_TABLE, null, cv);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public static Alarm getAlarm(String taskId){

//        db.execSQL("CREATE TABLE IF NOT EXISTS " + ALARM_TABLE + " ( "
//                + COLUMN_ALARM_ID + " INTEGER primary key autoincrement, "
//                + COLUMN_ALARM_ACTIVE + " INTEGER NOT NULL, "
//                + COLUMN_ALARM_TIME + " TEXT NOT NULL, "
//                + COLUMN_ALARM_DAYS + " BLOB NOT NULL, "
//                + COLUMN_ALARM_DIFFICULTY + " INTEGER NOT NULL, "
//                + COLUMN_ALARM_TONE + " TEXT NOT NULL, "
//                + COLUMN_ALARM_VIBRATE + " INTEGER NOT NULL, "
//                + COLUMN_ALARM_TASK_ID + " INTEGER , "
//                + COLUMN_ALARM_NAME + " TEXT NOT NULL)");
        Log.d("taskid",taskId);
        Alarm alarm=new Alarm();
        Cursor cursor=database.rawQuery("select * from " + DataBaseHelper.ALARM_TABLE + " where "+DataBaseHelper.TASK_ID+" ='"+taskId+"';",null,null);
        cursor.moveToFirst();
        alarm.setAlarmTaskId(cursor.getInt(7) + "");
        Log.d("getalarmId",cursor.getInt(7)+"");
        alarm.setAlarmActive(cursor.getInt(1) == 1 ? true : false);
        alarm.setAlarmTime(cursor.getString(2));
        byte[] repeatDaysBytes = cursor.getBlob(3);

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
                repeatDaysBytes);
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(
                    byteArrayInputStream);
            Alarm.Day[] repeatDays;
            Object object = objectInputStream.readObject();
            if (object instanceof Alarm.Day[]) {
                repeatDays = (Alarm.Day[]) object;
                alarm.setDays(repeatDays);
            }
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        alarm.setDifficulty(Alarm.Difficulty.values()[cursor.getInt(4)]);
        alarm.setAlarmTonePath(cursor.getString(5));
        alarm.setVibrate(cursor.getInt(6) == 1);
        alarm.setAlarmName(cursor.getString(8));
        Log.d("alarmname",cursor.getString(8));
        return alarm;
    }
    public static int update(Alarm alarm) {
        Log.d("updateAlarm",alarm.getAlarmTaskId()+" "+alarm.getAlarmName()+alarm.getAlarmTimeString());
        ContentValues cv = new ContentValues();
        cv.put(DataBaseHelper.COLUMN_ALARM_ACTIVE, alarm.getAlarmActive());
        cv.put(DataBaseHelper.COLUMN_ALARM_TIME, alarm.getAlarmTimeString());

        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = null;
            oos = new ObjectOutputStream(bos);
            oos.writeObject(alarm.getDays());
            byte[] buff = bos.toByteArray();

            cv.put(DataBaseHelper.COLUMN_ALARM_DAYS, buff);

        } catch (Exception e){
        }

        cv.put(DataBaseHelper.COLUMN_ALARM_DIFFICULTY, alarm.getDifficulty().ordinal());
        cv.put(DataBaseHelper.COLUMN_ALARM_TONE, alarm.getAlarmTonePath());
        cv.put(DataBaseHelper.COLUMN_ALARM_VIBRATE, alarm.getVibrate());
        cv.put(DataBaseHelper.COLUMN_ALARM_NAME, alarm.getAlarmName());

        return getDatabase().update(DataBaseHelper.ALARM_TABLE, cv, "id=" + alarm.getAlarmTaskId(), null);
    }


    public static int deleteEntry(String name){

        return getDatabase().delete(DataBaseHelper.ALARM_TABLE, DataBaseHelper.COLUMN_ALARM_NAME + " = ?", new String[]{name});
    }

    public static int deleteAll(){
        return getDatabase().delete(DataBaseHelper.ALARM_TABLE, "1", null);
    }

    public static Cursor getCursor() {
        // TODO Auto-generated method stub
        String[] columns = new String[] {
                DataBaseHelper.COLUMN_ALARM_ID,
                DataBaseHelper.COLUMN_ALARM_ACTIVE,
                DataBaseHelper.COLUMN_ALARM_TIME,
                DataBaseHelper.COLUMN_ALARM_DAYS,
                DataBaseHelper.COLUMN_ALARM_DIFFICULTY,
                DataBaseHelper.COLUMN_ALARM_TONE,
                DataBaseHelper.COLUMN_ALARM_VIBRATE,
                DataBaseHelper.COLUMN_ALARM_NAME
        };
        return getDatabase().query(DataBaseHelper.ALARM_TABLE, columns, null, null, null, null,
                null);
    }



    public ArrayList<Task> getAllData(){

        ArrayList<Task> taskList=new ArrayList<Task>();

        String selectQuery="SELECT * FROM "+DataBaseHelper.TABLE_NAME + " ORDER BY "+DataBaseHelper.TAST_FLAG + " DESC ";

        SQLiteDatabase sqLiteDatabase=helper.getWritableDatabase();

        Cursor cursor=sqLiteDatabase.rawQuery(selectQuery,null);
        if (cursor.moveToFirst()) {
           do {
               Task task=new Task();
               task.setId(cursor.getLong(cursor.getColumnIndexOrThrow(DataBaseHelper.TASK_ID))+"");
                task.setTaskName(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.TASK_NAME)));
                task.setTaskTime(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.TASK_TIME)));
                task.setTaskDate(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseHelper.TASK_DATE)));
                task.setCheckedFlag(cursor.getLong(cursor.getColumnIndexOrThrow(DataBaseHelper.TAST_FLAG)));


                taskList.add(task);

            } while (cursor.moveToNext());
        }
        sqLiteDatabase.close();
        return taskList;
    }
    public static List<Alarm> getAll() {
        List<Alarm> alarms = new ArrayList<Alarm>();
        Cursor cursor = DataBase.getCursor();
        if (cursor.moveToFirst()) {

            do {

                Alarm alarm = new Alarm();
                alarm.setId(cursor.getInt(0));
                alarm.setAlarmActive(cursor.getInt(1) == 1);
                alarm.setAlarmTime(cursor.getString(2));
                byte[] repeatDaysBytes = cursor.getBlob(3);

                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
                        repeatDaysBytes);
                try {
                    ObjectInputStream objectInputStream = new ObjectInputStream(
                            byteArrayInputStream);
                    Alarm.Day[] repeatDays;
                    Object object = objectInputStream.readObject();
                    if (object instanceof Alarm.Day[]) {
                        repeatDays = (Alarm.Day[]) object;
                        alarm.setDays(repeatDays);
                    }
                } catch (StreamCorruptedException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

                alarm.setDifficulty(Alarm.Difficulty.values()[cursor.getInt(4)]);
                alarm.setAlarmTonePath(cursor.getString(5));
                alarm.setVibrate(cursor.getInt(6) == 1);
              alarm.setAlarmName(cursor.getString(7));
//                alarm.setAlarmName(Task.getInstance().getTaskName());

                alarms.add(alarm);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return alarms;
    }

    static class DataBaseHelper extends SQLiteOpenHelper{

        private static final String DATABASE_NAME="TaskDataBase.db";
        private static final String TABLE_NAME="TaskHistory";
        private static final int DATABASE_VERSION=1;
        private static final String TASK_ID="id";
        private static final String TASK_NAME="taskName";
        private static final String TASK_TIME="taskTime";
        private static final String TASK_DATE="taskDate";
        public static final String TAST_FLAG="checkedFlag";
        public static final String ALARM_TABLE = "alarm";
        public static final String COLUMN_ALARM_ID = "alarmId";
        public static final String COLUMN_ALARM_ACTIVE = "alarmActive";
        public static final String COLUMN_ALARM_TIME = "alarmTime";
        public static final String COLUMN_ALARM_DAYS = "alarmDays";
        public static final String COLUMN_ALARM_DIFFICULTY = "alarmDifficulty";
        public static final String COLUMN_ALARM_TONE = "alarmTone";
        public static final String COLUMN_ALARM_VIBRATE = "alarmVibrate";
        public static final String COLUMN_ALARM_NAME = "alarmName";
        public static final String COLUMN_ALARM_TASK_ID="id";
        private static final String CREATE_TABLE="CREATE TABLE "+TABLE_NAME+"("+TASK_ID+" INTEGER PRIMARY KEY , "+TASK_NAME+" TEXT, "+TASK_TIME+" TEXT, "+TASK_DATE+" TEXT, "+TAST_FLAG+" INT "+")";
      //  private static final String ALARM_TABLE="ALARM TABLE "+ ALARM_NAME + " ( " + COLUMN_ALARM_ID + " INTEGER PRIMARY KEY, " + COLUMN_ALARM_ACTIVE + " INTEGER, " + COLUMN_ALARM_TIME + " TEXT, " + COLUMN_ALARM_DAYS + " BLOB, " + COLUMN_ALARM_DIFFICULTY + " INTEGER, " + COLUMN_ALARM_TONE + " TEXT, " + COLUMN_ALARM_VIBRATE + " INTEGER, " + COLUMN_ALARM_NAME + " TEXT)";
        private static final String DROP_TABLE="DROP TABLE IF EXISTS "+TABLE_NAME;
        private Context context;
        public DataBaseHelper(Context context) {
            super(context, DATABASE_NAME,null,DATABASE_VERSION);
            this.context=context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {



            db.execSQL(CREATE_TABLE);
            db.execSQL("CREATE TABLE IF NOT EXISTS " + ALARM_TABLE + " ( "
                    + COLUMN_ALARM_ID + " INTEGER primary key autoincrement, "
                    + COLUMN_ALARM_ACTIVE + " INTEGER NOT NULL, "
                    + COLUMN_ALARM_TIME + " TEXT NOT NULL, "
                    + COLUMN_ALARM_DAYS + " BLOB NOT NULL, "
                    + COLUMN_ALARM_DIFFICULTY + " INTEGER NOT NULL, "
                    + COLUMN_ALARM_TONE + " TEXT NOT NULL, "
                    + COLUMN_ALARM_VIBRATE + " INTEGER NOT NULL, "
                    + COLUMN_ALARM_TASK_ID + " INTEGER , "
                    + COLUMN_ALARM_NAME + " TEXT NOT NULL)");


        }

        public void selectAll()
        {


            SQLiteDatabase sqLiteDatabase=this.getWritableDatabase();
            Cursor c= sqLiteDatabase.rawQuery("select "+TASK_NAME+" from  "+TABLE_NAME,null);
            if(c.getCount()>0)
            {
                c.moveToFirst();
               StringBuilder sb=new StringBuilder();
                while(c.moveToNext())
                {
                    sb.append(c.getString(0));
                }
                Toast.makeText(context,""+c.getString(0),Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            db.execSQL(DROP_TABLE);
            onCreate(db);

        }

    }
}
