package com.example.webwerks.todolisttaskdemo.model;

import java.io.Serializable;

/**
 * Created by webwerks on 16/12/15.
 */
public class Task implements Serializable {

    private static Task newTask;
    private String taskName;
    private String taskTime;
    private String taskDate;
    private String id=System.currentTimeMillis()+"";
    private long checkedFlag;
    private boolean completeTaskFlag;
    private Alarm alarm;

    public Alarm getAlarm() {
        return alarm;
    }

    public void setAlarm(Alarm alarm) {
        this.alarm = alarm;
    }


    public Task() {
    }

    public  void setNewTask(){
        newTask=null;
    }

    public String getTaskName() {
        return taskName;
    }

    public static void clearInstance() {
        newTask = null;
    }

    public String getTaskTime() {
        return taskTime;
    }

    public String getTaskDate() {
        return taskDate;
    }

    public long getCheckedFlag() {
        return checkedFlag;
    }

    public String getId() {
        return id;
    }

    public boolean isCompleteTaskFlag() {
        return completeTaskFlag;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public void setTaskTime(String taskTime) {
        this.taskTime = taskTime;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCheckedFlag(long flag) {
        this.checkedFlag = flag;
    }

    public void setTaskDate(String taskDate) {
        this.taskDate = taskDate;
    }

    public void setCompleteTaskFlag(boolean completeTaskFlag) {

        this.completeTaskFlag = completeTaskFlag;
    }

    public static Task getInstance() {
        if (newTask == null)
            newTask = new Task();

        return newTask;
    }


}
