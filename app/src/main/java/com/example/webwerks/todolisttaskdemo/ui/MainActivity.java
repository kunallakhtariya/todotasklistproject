package com.example.webwerks.todolisttaskdemo.ui;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.ViewTarget;
import com.example.webwerks.todolisttaskdemo.R;
import com.example.webwerks.todolisttaskdemo.TaskListApplication;
import com.example.webwerks.todolisttaskdemo.adapter.TaskListAdapter;
import com.example.webwerks.todolisttaskdemo.alarm.receiver.AlarmServiceBroadcastReciever;
import com.example.webwerks.todolisttaskdemo.database.DataBase;
import com.example.webwerks.todolisttaskdemo.fragment.SetDateTimeDialogFragment;
import com.example.webwerks.todolisttaskdemo.fragment.UpdateDialogFragment;
import com.example.webwerks.todolisttaskdemo.interfaces.ActionReceiver;
import com.example.webwerks.todolisttaskdemo.listener.DateTimeListener;
import com.example.webwerks.todolisttaskdemo.model.Alarm;
import com.example.webwerks.todolisttaskdemo.model.Task;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemLongClickListener,DateTimeListener,AdapterView.OnItemClickListener,ActionReceiver{

    private ListView listView;
    private DataBase dataBase;
    private AlertDialog.Builder alertDialog;
    private ArrayList<Task> arrayList;
    private TaskListAdapter taskListAdapter;
    private LinearLayout linearLayout;
    private EditText editText;
    private ImageView alarmButton;
    private FragmentManager fragmentManager;
    private Menu myMenu;
    private CheckBox addFirst;
   private Task taskObject;
    private   long    second;
    private String taskDate;
    private String taskTime;
    AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private TimePicker timePicker;
    private static MainActivity mainActivity;
    private SimpleDateFormat dateFormatter;
    private TextView alaramTextView;
    private int hour;
    private int minute;
    private Alarm alarm;

    public static MainActivity instance(){
        return mainActivity;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mainActivity=this;
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        linearLayout=(LinearLayout)findViewById(R.id.linear_layout);
        editText=(EditText)findViewById(R.id.addTask1);
        addFirst=(CheckBox)findViewById(R.id.checkBox_first_add);
        alarmManager=(AlarmManager) getSystemService(ALARM_SERVICE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        initViews();
        callMathAlarmScheduleService();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("after Fragment", "afterFragment");
        setUpListView();

    }


    private void initViews() {

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemLongClickListener(this);
        listView.setOnItemClickListener(this);

        addFirst.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    second = System.currentTimeMillis();
                    Log.e("second", second + "");

                }
            }
        });
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence == null || charSequence.length() == 0) {
                 myMenu.findItem(R.id.action_add).setEnabled(false);
                } else {
                   myMenu.findItem(R.id.action_add).setEnabled(true);

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });

        Glide.with(this).load(R.drawable.bg).skipMemoryCache(true).into(new ViewTarget<LinearLayout,
                GlideDrawable>(linearLayout) {
            @Override
            public void onResourceReady(GlideDrawable resource, GlideAnimation anim) {
                LinearLayout myView = this.view;
                myView.setBackgroundDrawable(resource);
                // Set your resource on myView and/or start your animation here.
            }
        });
    }

    private void setUpListView() {
        taskListAdapter=new TaskListAdapter(this,0, TaskListApplication.newInstance().getCurrentTaskList(),this);
        listView.setAdapter(taskListAdapter);

    }

    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.menu_item_save).setVisible(false);
        menu.findItem(R.id.menu_item_delete).setVisible(false);

            return true;

    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        myMenu = menu;

        myMenu.findItem(R.id.action_add).setEnabled(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.action_add:

                    insert();
                    return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setAlarm(View view){
        Log.e("alarmActivity", "alarmActivity");
        Intent newAlarmIntent = new Intent(this, AlarmPreferencesActivity.class);
        startActivity(newAlarmIntent);
    }





    public void insert(){

        taskObject=Task.getInstance();
        alarm=Alarm.getInstance();
        alarm.setAlarmTaskId(taskObject.getId());
        Log.e("insertalarmid",alarm.getAlarmTaskId()+"");
        Log.d("insert task object", taskObject + "");
        Log.d("insert alarm object",taskObject.getAlarm()+"");
        taskObject.setTaskName(editText.getText().toString());
         taskObject.setTaskDate(taskDate);
         taskObject.setTaskTime(taskTime);
         taskObject.setCheckedFlag(second);

         editText.setText("");
         addFirst.setChecked(false);
         long id= TaskListApplication.newInstance().insertTask(taskObject);
          Log.d("TaskID",""+id);
         if (id<0){
             Toast.makeText(getApplication(), "Not Done", Toast.LENGTH_SHORT).show();
         }else {
             if (taskObject.getCheckedFlag()!=0){
                 TaskListApplication.newInstance().changeTaskList();
                 taskListAdapter=new TaskListAdapter(this,0, TaskListApplication.newInstance().getCurrentTaskList(),this);
                 listView.setAdapter(taskListAdapter);
                 taskListAdapter.notifyDataSetChanged();
                 Log.e("secondIf", second + "");
                 second = 0;
                 Log.e("secondAfter",second+"");

             }

             Toast.makeText(getApplication(), "Done", Toast.LENGTH_SHORT).show();
             taskListAdapter.notifyDataSetChanged();
         }

     }
    public void setDateTime(View view) {


        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog datePickerDialogatePickerDialog = new DatePickerDialog(this,R.style.DialogTheme,new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                taskDate=dateFormatter.format(newDate.getTime());
                Toast.makeText(getApplicationContext(),dateFormatter.format(newDate.getTime()),Toast.LENGTH_SHORT).show();
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialogatePickerDialog.show();

    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {
        Log.d("onlongclick","onlongclick");
        Task task=(Task)taskListAdapter.getItem(position);
          final String name= task.getTaskName();
        alertDialog = new AlertDialog.Builder(MainActivity.this);
        alertDialog.setMessage("Do you want to delete ?");
        alertDialog.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                DataBase.deleteEntry(name);
                MainActivity.this.callMathAlarmScheduleService();
               TaskListApplication.newInstance().deleteTask(position);
               taskListAdapter.notifyDataSetChanged();


            }
        });
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                alertDialog.setCancelable(true);
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.show();
        return true;

    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Task task=(Task)taskListAdapter.getItem(position);

        fragmentManager=getSupportFragmentManager();
        UpdateDialogFragment dialogFragment=UpdateDialogFragment.newInstance(this, task);
        dialogFragment.show(fragmentManager, "Update Task");

        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);


    }


    @Override
    public void onActionComplete(int requestCode) {
        switch (requestCode){
            case 1:
                Toast.makeText(this,"Record updated Successfully",Toast.LENGTH_SHORT).show();
                taskListAdapter.notifyDataSetChanged();
                break;
            case 2:
              TaskListApplication.newInstance().changeTaskList();
                taskListAdapter=new TaskListAdapter(this,0, TaskListApplication.newInstance().getCurrentTaskList(),this);
                listView.setAdapter(taskListAdapter);
                taskListAdapter.notifyDataSetChanged();
                break;
            case 3:
                Toast.makeText(this,"Complete Task",Toast.LENGTH_SHORT).show();
                taskListAdapter.notifyDataSetChanged();
                break;
        }

    }

    @Override
    public void onDateSet(String date) {
        taskDate=date;

    }

    @Override
    public void onTimeSet(int hour, int minute) {
     this.hour=hour;
        this.minute=minute;
        String time=hour+":"+minute;
        taskTime=time;
    }
    public void callMathAlarmScheduleService() {
        Intent mathAlarmServiceIntent = new Intent(this, AlarmServiceBroadcastReciever.class);

        sendBroadcast(mathAlarmServiceIntent, null);
    }

}



