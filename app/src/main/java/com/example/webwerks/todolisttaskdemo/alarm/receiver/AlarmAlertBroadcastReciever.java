package com.example.webwerks.todolisttaskdemo.alarm.receiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.example.webwerks.todolisttaskdemo.R;
import com.example.webwerks.todolisttaskdemo.model.Alarm;
import com.example.webwerks.todolisttaskdemo.model.Task;
import com.example.webwerks.todolisttaskdemo.ui.AlarmAlertActivity;
import com.example.webwerks.todolisttaskdemo.alarm.StaticWakeLock;
import com.example.webwerks.todolisttaskdemo.ui.MainActivity;

/**
 * Created by webwerks on 25/1/16.
 */
public class    AlarmAlertBroadcastReciever extends BroadcastReceiver {
    private NotificationManager notificationManager;
    private Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context=context;
        Log.d(this.getClass().getSimpleName(), "BroadcastReciever");
        Intent mathAlarmServiceIntent = new Intent(
                context,
                AlarmServiceBroadcastReciever.class);
        context.sendBroadcast(mathAlarmServiceIntent, null);
        String  receiveNumbers = intent.getStringExtra("alarmName");
        Log.e("alarmName",receiveNumbers);
       sendNotification(receiveNumbers); ;
    }
    public void sendNotification(String msg){

        notificationManager=(NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);


        PendingIntent pendingIntent =PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), 0);

        NotificationCompat.Builder builder=new NotificationCompat.Builder(context).setContentTitle("Alaram").setSmallIcon(R.drawable.abc).setStyle(new NotificationCompat.BigTextStyle().bigText(msg)).setContentText(msg);

        builder.setContentIntent(pendingIntent);
        notificationManager.notify(1, builder.build());
        Log.d("AlaramService", "Notification sent");
    }

}
