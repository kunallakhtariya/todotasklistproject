package com.example.webwerks.todolisttaskdemo.interfaces;

/**
 * Created by webwerks on 5/1/16.
 */
public interface ActionReceiver {

    public void onActionComplete(int requestCode);

}
