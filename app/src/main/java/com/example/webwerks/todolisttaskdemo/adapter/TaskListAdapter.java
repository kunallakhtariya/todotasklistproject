package com.example.webwerks.todolisttaskdemo.adapter;

import android.content.Context;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.webwerks.todolisttaskdemo.R;
import com.example.webwerks.todolisttaskdemo.TaskListApplication;
import com.example.webwerks.todolisttaskdemo.database.DataBase;
import com.example.webwerks.todolisttaskdemo.interfaces.ActionReceiver;
import com.example.webwerks.todolisttaskdemo.model.Task;

import java.util.ArrayList;
import java.util.concurrent.Delayed;

/**
 * Created by webwerks on 16/12/15.
 */
public class TaskListAdapter extends ArrayAdapter<Task> {

    DataBase dataBase;
    ActionReceiver mReceiver;

    public TaskListAdapter(Context context, int resource, ArrayList<Task> taskArrayAdapter,ActionReceiver listner) {
        super(context, resource, taskArrayAdapter);
        mReceiver=listner;

    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {




        Task task = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_task_list, parent, false);
        }
        final CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
        checkBox.setTag(getItem(position));
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                dataBase = new DataBase(getContext());
                Task task = (Task) buttonView.getTag();
                if (isChecked) {
                    long second = System.currentTimeMillis();
                    task.setCheckedFlag(second);
                    TaskListApplication.newInstance().insertFlag(task);
                    mReceiver.onActionComplete(2);


                }
            }
        });
        final CheckBox completeTask = (CheckBox) convertView.findViewById(R.id.complete_task);
        completeTask.setTag(getItem(position));
        completeTask.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                dataBase = new DataBase(getContext());
                Task task = (Task) buttonView.getTag();

               task.setCompleteTaskFlag(true);
                if (isChecked) {

                    TaskListApplication.newInstance().completeTask(task);
                    mReceiver.onActionComplete(3);
                    if (task.isCompleteTaskFlag()){
                        completeTask.setChecked(false);
                    }

                }
            }
        });
        TextView tvName = (TextView) convertView.findViewById(R.id.tv_name);
      TextView tvDate=(TextView)convertView.findViewById(R.id.tv_date);
        TextView tvTime=(TextView)convertView.findViewById(R.id.tv_time);
        tvName.setText(task.getTaskName());
        tvDate.setText(task.getTaskDate());
        tvTime.setText(task.getTaskTime());
        convertView.setTag(task);
        return convertView;
    }
}

